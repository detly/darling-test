use darling::{ast::Fields, FromDeriveInput};
use proc_macro::TokenStream;
use syn::{parse_macro_input, spanned::Spanned, DeriveInput, Generics, Ident};

#[proc_macro_derive(DarlingTest, attributes(darling_test))]
pub fn derive_darling_test(input: TokenStream) -> TokenStream {
    // Get derive input.
    let input = parse_macro_input!(input as syn::DeriveInput);

    // Wrap the real function so we can use syn's result type and proc_macro2
    // types.
    create_impls(input)
        .unwrap_or_else(|e| e.to_compile_error())
        .into()
}

fn create_impls(input: DeriveInput) -> syn::Result<proc_macro2::TokenStream> {
    let darling_test_derive = DarlingTestDerive::from_derive_input(&input)?;

    // Destructure the derive data, bailing if it's an enum.
    let Some(_data @ Fields { .. }) = darling_test_derive.data.take_struct() else {
        return Err(syn::Error::new(input.span(), "only structs are supported"));
    };

    let span = input.span();

    // Pretend error.
    Err(syn::Error::new(span, "these fields are bad"))
}

#[derive(Debug, FromDeriveInput)]
#[allow(unused)]
struct DarlingTestDerive {
    ident: Ident,
    generics: Generics,
    data: darling::ast::Data<(), ()>,
}
