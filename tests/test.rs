use darling_test::DarlingTest;

#[test]
fn darling_test_struct() {
    /// Some doc comment.
    #[derive(Clone, Debug, Default, DarlingTest, PartialEq)]
    struct DarlingTestStruct(#[darling_test] bool, usize);
}

#[test]
fn darling_test_enum() {
    /// Some doc comment.
    #[derive(Clone, Debug, DarlingTest, PartialEq)]
    enum DarlingTestEnum {
        One,
        Two,
    }
}
